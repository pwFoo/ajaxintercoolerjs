$( document ).ready(function() {
    /*
    Intercooler.ready(function (elt) {

    });
    */
    /*
    $('body').on('pwEvent', function (icEvent, data) {
        console.log(data);
    });
    */

    /**
     * event handler async scripts
     */
    $('body').on('icScripts', function (icEvent, data) {
        // Remove old / unsused scripts
        $('head').children('script.current').remove();

        // No scripts to load? stop here...
        if (data === undefined) {
            return;
        }

        // load new current page scripts
        $.each(data, function (key, value) {
            if ($('script[src="' + this + '"]').length < 1) {
                $.ajax({
                    url: this,
                    dataType: 'script',
                    cache: true // to prevent multiple file download
                });
            }
        });
    });

    /**
     * event handler async styles
     */
    $('body').on('icStyles', function (icEvent, data) {
        var fileArray = data;
        // remove unused stylesheet (not in the current css array)
        $('head').children('link.current').each(function () {
            if (fileArray == undefined) {    // no files to keep, so we can remove it here...
                $(this).remove();
            } else {    // check if the file is to keep or remove
                var refArray = $.map(fileArray, function(val, key) { return val; });    // prepare associative array for inArray...
                if ($.inArray(this.href, refArray) == -1) {
                    $(this).remove();
                }
            }
        });

        // No styles to load? stop here...
        if (data === undefined) {
            return;
        }

        // load new current page styles
        $.each(data, function (key, value) {
            if ($('link[href="' + this + '"]').length < 1) {
                loadCSS(this);
                // move to head and add class "current"
                $('body > link').addClass('current').detach().appendTo('head');
            }

        });
    });
});
