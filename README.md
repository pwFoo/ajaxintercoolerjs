# AjaxIntercoolerJS module #

**AjaxIntercoolerJS** is a Processwire CMS / CMF module and adds [IntercoolerJS](http://intercoolerjs.org/) ajax functionalities.

## Documentation and support ##

* [Documentation](https://bitbucket.org/pwFoo/ajaxintercoolerjs/wiki/Documentation)
* [Issues](https://bitbucket.org/pwFoo/ajaxintercoolerjs/issues?status=new&status=open) and Processwire support forum
* [Source code](https://bitbucket.org/pwFoo/ajaxintercoolerjs/src/master)
* [Download current release](https://bitbucket.org/pwFoo/ajaxintercoolerjs/get/master.zip)